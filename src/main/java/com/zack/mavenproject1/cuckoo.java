package com.zack.mavenproject1;



/**
 *
 * @author acer
 */
public class cuckoo {

    //Declare the scope of the variable.
    private int key;
    private String value;

    cuckoo[] T1 = new cuckoo[75];
    cuckoo[] T2 = new cuckoo[75];

    //Executing parameters within a class
    public cuckoo(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public cuckoo() {

    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public int hash1(int key) {
        return key % T1.length;
    }

    public int hash2(int key) {
        return (key / 75) % T2.length;
    }

    public void put(int key, String value) { //method for put values ​​to each table

        if (T1[hash1(key)] != null && T1[hash1(key)].key == key) {
            T1[hash1(key)].key = key;
            T1[hash1(key)].value = value;
            return;
        }
        if (T2[hash2(key)] != null && T2[hash2(key)].key == key) {
            T2[hash2(key)].key = key;
            T2[hash2(key)].value = value;
            return;
        }
        int i = 0;

        while (true) {
            if (i == 0) {
                if (T1[hash1(key)] == null) {
                    T1[hash1(key)] = new cuckoo();
                    T1[hash1(key)].key = key;
                    T1[hash1(key)].value = value;
                    return;
                }
            } else {
                if (T2[hash2(key)] == null) {
                    T2[hash2(key)] = new cuckoo();
                    T2[hash2(key)].key = key;
                    T2[hash2(key)].value = value;
                    return;
                }
            }

            if (i == 0) {
                String tempValue = T1[hash1(key)].value;
                int tempKey = T1[hash1(key)].key;
                T1[hash1(key)].key = key;
                T1[hash1(key)].value = value;
                key = tempKey;
                value = tempValue;

            } else { 
                String tempValue = T2[hash2(key)].value;
                int tempKey = T2[hash2(key)].key;
                T2[hash2(key)].key = key;
                T2[hash2(key)].value = value;
                key = tempKey;
                value = tempValue;
            }
            i = (i + 1) % 2;
        }
    }

    public void add(int key, String value) {  //method for adding values ​​to each table
        if (T1[hash1(key)] != null && T1[hash1(key)].key == key) {
            T1[hash1(key)].key = key;
            T1[hash1(key)].value = value;
            return;
        }
        if (T2[hash2(key)] != null && T2[hash2(key)].key == key) {
            T2[hash2(key)].key = key;
            T2[hash2(key)].value = value;
            return;
        }
        int i = 0;

        while (true) {
            if (i == 0) {
                if (T1[hash1(key)] == null) {
                    T1[hash1(key)] = new cuckoo();
                    T1[hash1(key)].key = key;
                    T1[hash1(key)].value = value;
                    return;
                }
            } else {
                if (T2[hash2(key)] == null) {
                    T2[hash2(key)] = new cuckoo();
                    T2[hash2(key)].key = key;
                    T2[hash2(key)].value = value;
                    return;
                }
            }

            if (i == 0) {
                String tempValue = T1[hash1(key)].value;
                int tempKey = T1[hash1(key)].key;
                T1[hash1(key)].key = key;
                T1[hash1(key)].value = value;
                key = tempKey;
                value = tempValue;

            } else {
                String tempValue = T2[hash2(key)].value;
                int tempKey = T2[hash2(key)].key;
                T2[hash2(key)].key = key;
                T2[hash2(key)].value = value;
                key = tempKey;
                value = tempValue;
            }
            i = (i + 1) % 2;
        }
    }

    public cuckoo get(int key) {
        if (T1[hash1(key)] != null && T1[hash1(key)].key == key) {
            return T1[hash1(key)];
        }
        if (T2[hash2(key)] != null && T2[hash2(key)].key == key) {
            return T2[hash2(key)];
        }
        return null;
    }

    public cuckoo delete(int key) {  //method for delete values ​​to each table
        if (T1[hash1(key)] != null && T1[hash1(key)].key == key) {
            cuckoo tmp = T1[hash1(key)];
            T1[hash1(key)] = null;
            return tmp;
        }
        if (T1[hash2(key)] != null && T1[hash2(key)].key == key) {
            cuckoo tmp = T1[hash2(key)];
            T1[hash2(key)] = null;
            return tmp;
        }
        return null;
    }

    @Override
    public String toString() {   //return message display
        return "Key -> " + key + " Value -> " + value;
    }

}
